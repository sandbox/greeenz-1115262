<?php
// $Id:$

/** 
 * @file
 * Administration page callbacks for the drconnect module.
 */

/** 
 * Form builder. Configure drconnect.
 *
 * @see system_settings_form().
 */
function drconnect_admin_settings() {
    global $base_url;
    $module_path = drupal_get_path('module', 'drconnect') .'/images/';
    
    $form['global'] = array(
            '#type' => 'fieldset',
            '#title' => t('Draugiem API Settings'),
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
    );
    $form['global']['drconnect_key'] = array(
            '#type' => 'textfield',
            '#title' => t('Draugiem application ID'),
            '#default_value' => variable_get('drconnect_key', NULL),
            '#description' => t('Your application ID number'),
    );
    
    $form['global']['drconnect_skey'] = array(
            '#type' => 'textfield',
            '#title' => t('Draugiem API key'),
            '#default_value' => variable_get('drconnect_skey', NULL),
            '#description' => t('Do not share your API key with anyone'),
    );
    
    $form['global']['drconnect_connect_url'] = array(
            '#type' => 'textfield',
            '#title' => t('Connect url'),
            '#description' => t('Your site\'s main URL'),
            '#default_value' => variable_get('drconnect_connect_url', $base_url),
    );
    
    $form['site'] = array(
            '#type' => 'fieldset',
            '#title' => t('Settings for !site', array('!site' => variable_get('site_name', t('Your Site')))),
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
    );
    
    $form['site']['drconnect_invite_msg'] = array(
            '#type' => 'textfield',
            '#title' => t('Invite message'),
            '#default_value' => variable_get('drconnect_invite_msg', t('Enjoy Draugiem.pase')),
    );
    
    $form['site']['drconnect_invite_name'] = array(
            '#type' => 'textfield',
            '#title' => t('Your site name'),
            '#default_value' => variable_get('drconnect_invite_name', variable_get('site_name', NULL)),
    );
    
    $form['site']['drconnect_new_user'] = array(
            '#type' => 'checkbox',
            '#title' => t('Show link option'),
            '#default_value' => variable_get('drconnect_new_user', 1),
            '#description' => t('Show new Draugiem.pase users option to link with existing site account'),
    );
    
    
    $form = system_settings_form($form);
    return $form;
}
